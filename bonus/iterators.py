import array
def file_iterator(file,n=100):
    with open(file) as f:
        while 1:
            chunk=f.read(n)
            if not chunk:
                break
            yield chunk

