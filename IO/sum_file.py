from miniproject.functions import primes
from collections import defaultdict
from urllib.request import urlopen
import re
import heapq
from bs4 import BeautifulSoup


def sum_numbers(file):
    with open(file) as f:
        local_sum = sum([int(el) for el in f.readlines()])

    return local_sum


def store_primes(file, number):
    with open(file, 'w') as f:
        for el in primes.ciur(number):
            f.write(str(el) + '\n')


def dict_from_file(file):
    d = defaultdict(int)
    with open(file) as f:
        for line in f:
            words = line.split()
            for word in words:
                d[word] += 1

    return d


def find_text(file):
    d = dict_from_file(file)
    rez = []
    for word in d:
        if d[word] == 1:
            rez.append(word)

    return rez


def merge_lists_in_one(lists):
    result = []
    my_heap = []
    indexis = len(lists) * [0]

    for i, myList in enumerate(lists):
        heapq.heappush(my_heap, (myList[0], i))

    while my_heap:
        element, list_number = heapq.heappop(my_heap)
        result.append(element)
        indexis[list_number] += 1
        if indexis[list_number] == len(lists[list_number]):
            continue
        heapq.heappush(my_heap, (lists[list_number][indexis[list_number]], list_number))

    return result


def merge_files_in_one(*files):
    my_lists = [[] for file in files]
    for i, file in enumerate(files):
        with open(file) as f:
            for line in f:
                my_lists[i].append(int(line))
    return merge_lists_in_one(my_lists)


def extract_lines_from_url(url):
    rez = []
    connection = urlopen(url)

    soup = BeautifulSoup(connection, 'html.parser')
    for link in soup.find_all('a'):
        rez.append(link.get('href'))

    return rez


extract_lines_from_url('https://www.youtube.com/watch?v=3IRpfht3X50')
assert merge_files_in_one('IO/file1', 'IO/file2', 'IO/file3') == [1, 2, 3, 5, 5, 6, 9, 10, 11]
assert merge_lists_in_one([[1, 5, 9], [2, 3, 5], [6, 10, 11]]) == [1, 2, 3, 5, 5, 6, 9, 10, 11]
assert find_text('IO/text.txt') == ['marian', 'ionut', 'canibal', 'cercel']
assert sum_numbers('IO/file.txt') == 9

store_primes('IO/primes.txt', 100)
