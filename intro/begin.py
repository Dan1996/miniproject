import sys

def is_palindrom(src):
    src=int(src)
    csrc=src
    dest=0
    while src!=0:
        dest+=src%10
        dest*=10
        src//=10
    dest//=10
    if dest == csrc :
        return True
    return False

def all_pal(my_range):
    my_range=int(my_range)
    rez=[]
    for el in range(my_range+1):
        if is_palindrom(el):
            rez.append(el)
    return rez


