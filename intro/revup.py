import sys

def string_upper_reverse(s):
    dest=''
    while s:
        dest+=s[-1]
        s=s[:-1]
    return dest.upper()

if __name__=='__main__':
    print (string_upper_reverse(sys.argv[1]))


