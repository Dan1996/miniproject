import miniproject.intro.begin
import miniproject.intro.revup
from miniproject.intro import allpal


def print_ui():
    options={
        'revup': miniproject.intro.revup.string_upper_reverse,
        'pal': miniproject.intro.begin.is_palindrom,
        'apal': allpal.all_pal,
    }
    while 1:
        opt=input("Alegeti comanda: revup, pal , apal, end ")
        if opt == 'end':
            break
        nr=input("Introduceti datele ")
        print (options[opt](nr))

print_ui()