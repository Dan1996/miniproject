DEBUG, INFO, WARNING, ERROR, CRITICAL = 'debug', 'info', 'warning', 'error', 'critical'

levels = {
    DEBUG: 0,
    INFO: 1,
    WARNING: 2,
    ERROR: 3,
    CRITICAL: 4
}


def check_level(current_command, object_command):
    return levels[current_command] >= levels[object_command]


class Logger(object):
    def __init__(self, level=WARNING, filename='', stream=False):
        self.handlers = []
        self.level = level
        if filename != '':
            self.add_handler(FileHandler(filename).set_level(self.level))
        if stream:
            self.add_handler(StreamHandler().set_level(self.level))

    def set_level(self, level):
        if level not in levels:
            raise ValueError('level {} not valid ({})'.format(level, ','.join(levels.keys())))
        self.level = level

    def add_handler(self, handler):
        self.handlers.append(handler)

    def debug(self, message):
        self.log(message, level=DEBUG)

    def info(self, message):
        self.log(message, level=INFO)

    def warning(self, message):
        self.log(message, level=WARNING)

    def error(self, message):
        self.log(message, level=ERROR)

    def critical(self, message):
        self.log(message, level=CRITICAL)

    def log(self, message, level=INFO):
        if check_level(level, self.level):
            self._send_to_handlers(level, message)

    def _send_to_handlers(self, command, message):
        for handler in self.handlers:
            handler.write(command, message)


class Handler:
    def __init__(self):
        self.level = WARNING

    def set_level(self, level):
        if level not in levels:
            raise ValueError('level {} not valid ({})'.format(level, ','.join(levels.keys())))
        self.level = level

    def write(self, command, message):
        pass


class StreamHandler(Handler):
    def __init__(self):
        Handler.__init__(self)

    def write(self, command, message):
        if check_level(command, self.level):
            print(command, ' ', message)


class FileHandler(Handler):
    def __init__(self, file_name):
        Handler.__init__(self)
        self.file_name = file_name
        self.f = open(self.file_name, 'a')

    def write(self, command, message):
        if check_level(command, self.level):
            self.f.write(''.join(('\n', command, ' ', message)))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.f.close()

logger = Logger(level=DEBUG)
handler = StreamHandler()
handler_2 = FileHandler('example.log')
handler.set_level('debug')
logger.add_handler(handler)
logger.add_handler(handler_2)
logger.debug('eqweqwe')
logger.critical('wqeqweqw')



























