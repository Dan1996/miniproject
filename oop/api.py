import re

valid = re.compile(r"(^[^ =]+) *= *([^ ].*)")


def command_parse(i, line):
    m = valid.match(line)
    if m:
        key, value = m.group(1, 2)
        try:
            return key, int(value)
        except ValueError:
            pass

        try:
            return key, float(value)

        except ValueError:
            m = re.match(" *'([^']*)' *$", value)
            if m:
                return key, m.group(1)

            raise Exception('Syntax error at line ' + str(i + 1))

    raise Exception('Syntax error at line ' + str(i + 1))


def loads(bytes_like):
    d = {}
    for i, line in enumerate(bytes_like):
        line = line.strip()
        key, value = command_parse(i, line)
        d[key] = value

    return d


def dump(writtabble, d):
    for key, value in d.items():
        try:
            value = int(value)
            value = str(value)

        except ValueError:
            value = ''.join(("'", value, "'"))
        except TypeError:
            raise Exception(''.join(('Wrong type in dict ', str(type(value)))))

        api_content = ''.join((key, ' ', '=', ' ', value, '\n'))
        writtabble.write(api_content)

f = open('tampenie')
d = loads(f)
print(d)
d['danone'] = 43
f = open('destinatie', 'w')
dump(f, d)