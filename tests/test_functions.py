import time
import functions.decorators as decorators
import functions.emulators as emulators
import functions.fibo as fibo
import functions.merge as merge
import functions.prime as prime
import functions.primes as primes
import functions.sort as sort

def test_my_decorators():
    assert decorators.even_more_stupid(30)==20603361
    time.sleep(2)
    assert decorators.stupid_fibo(30)==832040
    actual_time=decorators.decor_many('now')
    assert actual_time>2 and actual_time <2.5

def test_emulators():
    assert emulators.map(lambda x: x * 2, [1, 2, 3]) == [2, 4, 6]
    assert emulators.filter(emulators.compare_if_positive, [-1, -2, 1, 2]) == [-1, -2]

def test_fibo():
    assert fibo.fibo_start(10) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
    assert fibo.fibo_iterative(10) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

def test_merge():
    assert merge.merge([1, 5, 7], [2, 4, 9]) == [1, 2, 4, 5, 7, 9]

def test_primes():
    assert prime.prime(11) == True
    assert primes.ciur(100)==[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47]

def test_sort():
    assert sort.dump_sort([7, 5, 9, 11, 3, -5]) == [-5, 3, 5, 7, 9, 11]
    assert sort.binary_search([-5, 3, 5, 7, 9, 11], 11) == 5