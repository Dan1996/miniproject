import argparse
import functions.fibo as fibo
import functions.sort as sort
import functions.primes as primes
import intro.begin as begin


if __name__ == '__main__':
    FIB = 'fib'
    RFIB = 'rfib'
    SORT = 'sort'
    PRIME = 'prime'
    CIUR = 'ciur'
    PAL = 'pal'
    APAL = 'apal'

    def make_option(command):
        return ''.join(('--', command))

    project_parser = argparse.ArgumentParser(prog='PROG')
    option_dict = {
        FIB: fibo.fibo_iterative,
        RFIB: fibo.fibo_start,
        SORT: sort.dump_sort,
        PRIME: primes.is_prime,
        CIUR: primes.ciur,
        PAL: begin.is_palindrom,
        APAL: begin.all_pal
    }

    project_parser.add_argument(make_option(FIB), type=int)
    project_parser.add_argument(make_option(RFIB), type=int)
    project_parser.add_argument(make_option(SORT), nargs='+', type=int)
    project_parser.add_argument(make_option(PRIME), type=int)
    project_parser.add_argument(make_option(CIUR), type=int)
    project_parser.add_argument(make_option(PAL), type=int)
    project_parser.add_argument(make_option(APAL), type=int)



    args = vars(project_parser.parse_args())
    for func, value in args.items():
        if value != None:
            print(func, ' ', option_dict[func](args[func]))


