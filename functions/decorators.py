import functools
import time

def counter(l = [0]):
   l[0] += 1
   return l[0]


def how_many_seconds_one(func):
    """measure in seconds exec of func"""
    @functools.wraps(func)
    def measure(*args, **kwargs):
        begin = time.time()
        result = func(*args, **kwargs)
        print(time.time() - begin)

        return result

    return measure


def decor_many(func, time_list=[]):

    def wrapple(*args):
        begin = time.time()
        result = func(*args)
        time_list.append(time.time() - begin)
        print(sum(time_list))

        return result

    return wrapple


class measure_time:
    time_history = []

    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        begin = time.time()
        result = self.func(*args, **kwargs)
        self.time_history.append(time.time() - begin)

        return result

    @staticmethod
    def total():
        return sum(measure_time.time_history)


def memoization(method):

    def wrapper_lru(func, _d={}):

        def inner_mem(*args, **kwargs):
            key = args

            if key not in _d:
                _d[key] = func(*args, **kwargs)

            return _d[key]

        return inner_mem

    if method.lower() == 'lru':
        return wrapper_lru
    else:
        raise ValueError('we dont know it: ' + method)

@memoization('lru')
def stupid_fibo(n):
    if n == 1 or n == 2:
        return 1
    return stupid_fibo(n-1) + stupid_fibo(n-2)


@how_many_seconds_one
def even_more_stupid(n):
    if n==1 or n==2 or n==3:
        return 1
    return even_more_stupid(n-2)+even_more_stupid(n-1)+even_more_stupid(n-3)


print(stupid_fibo(30))
