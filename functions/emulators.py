def map(func,iterable):
    return [func(el) for el in iterable]

def filter(func,iterable):
    return [el for el in iterable if func(el)]

def reduce(func,iterable,initializer):
    prev=initializer
    for el in iterable:
        rez=func(prev,el)
        prev=el
    return rez

def find_satisfying_sum(value):
    return reduce(lambda x,y:x+y,filter(lambda x:True if x%3==0 else False ,map(lambda x:x*x-1,range(value))),0)

def compare_if_positive(el):
    if el>0:
        return False
    return True

