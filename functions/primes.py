import sys


def ciur(nr):
    l = [True for nr in range(nr)]
    for i, el in enumerate(l):
        if el and i != 0 and i != 1:
            step = i
            for j in range(i * 2, len(l), step):
                l[j] = False
    return [i for i, el in enumerate(l) if el and i != 1 and i != 0]


def is_prime(nr):
    d = 2
    for el in range(nr // 2 + 1):
        if nr % d == 0:
            return False
    return True
