def dump_sort(iterable):
    for i in range(len(iterable) - 1):
        for j in range(i + 1, len(iterable)):
            if iterable[i] > iterable[j]:
                iterable[i], iterable[j] = iterable[j], iterable[i]
    return iterable


def binary_search(iterable, searched):
    a = 0
    b = len(iterable) - 1
    while a <= b:
        m = (a + b) // 2
        if iterable[m] == searched:
            return m
        elif iterable[m] < searched:
            a = m + 1
        else:
            b = m - 1


def merge(l1, l2):
    l = []
    i, j = 0, 0
    while j < len(l2) and i < len(l1):
        if l1[i] > l2[j]:
            l.append(l2[j])
            j += 1
        else:
            l.append(l1[i])
            i += 1
    l += l1[i:]
    l += l2[j:]
    return l
