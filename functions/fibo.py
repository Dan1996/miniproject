def fibo_start(n):
    l=[1,1]
    fibo_rec(l,2,n)
    return l

def fibo_rec(l,i,n):
    if len(l)==n:
        return
    l.append(l[i-1]+l[i-2])
    fibo_rec(l,i+1,n)

def fibo_iterative(n):
    l=[1,1]
    for i in range(2,n):
        l.append(l[i-1]+l[i-2])
    return l

